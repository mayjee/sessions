import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import {Container} from 'react-bootstrap';


// the 'App.js' component is where we usually import other custom components
// when putting two or more components together, we have to use a container for it to work properly
function App() {
  return (
    <>
      <AppNavbar/>
      <Container>
        <Home/>
        <Courses/>
      </Container>
      
    </>
  );
}

export default App;
