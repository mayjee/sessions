import {Button, Row, Col} from 'react-bootstrap';

export default function Banner() {
	return (

		<Row>
			<Col className="p-5 text-center">
				<h1>Suguru's Monkey Academy</h1>
				<p>Monkey for everyone!</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>

	)
}