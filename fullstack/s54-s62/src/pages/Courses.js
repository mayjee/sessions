import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses() {

	const courses = coursesData.map(course_item => {
		return (
			<CourseCard key={course_item.id} course={course_item} />
			// course -- prop
		)
	})

	return(

		<>
			<h1>Courses</h1>

			{/* to generate each data in the .map() function */}
			{ courses } 

			{/* hard coded */}
			{/* <CourseCard course={coursesData[0]}/>
			<CourseCard course={coursesData[1]}/> */}
		</>
	)
}