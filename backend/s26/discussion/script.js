// [SECTION] Arrays and Indexes

let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "Asus", null, undefined, {}];




// alternative way to write arrays for developers' UX

let my_tasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
	];



// reassigning array values

console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world";
console.log("Array after reassignment:");
console.log(my_tasks);




// [SECTION] reading from arrays

console.log(computer_brands[2], [4]);




// getting the length of an array
console.log(computer_brands.length);


// accessing last element in an array

let index_of_last_element = computer_brands.length - 1;

console.log(computer_brands[index_of_last_element]);





// [SECTION] array methods/array functions
// [sub-section] mutator methods

let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];

console.log("Current array:");
console.log(fruits);

fruits.push("Mango", "Cocomelon");
console.log("Updated array after push method:");
console.log(fruits);



// pop methos

// console.log(fruits.pop()); // output: cocomelon



console.log("Current array:");
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated array after pop method:");
console.log(fruits);

console.log("Removed fruit: " + removed_item);




// unshift method is for adding items to the beginning of an array
console.log("Current array:");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:");
console.log(fruits);




// shift method removes the item at the beginning of an array
console.log("Current array:");
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method:");
console.log(fruits);




// splice method replaces the items in an array according to the indicated index number
console.log("Current array:");
console.log(fruits);

// first number -- where the replacement starts
// second number -- the number of items to be replaced
// if the second number is not specified, it only adds the added items starting at the first number (index number)
fruits.splice(1, 2, "Lime", "Cherry");

console.log("Updated array after splice method:");
console.log(fruits);




// sort method -- arranges the items in an array alphabetically
// reverse method -- arranges the items in an array in a reverse order according to their index number

console.log("Current array:");
console.log(fruits);

fruits.sort();
fruits.reverse();

console.log("Updated array after sort method:");
console.log(fruits);






// [sub-section] non-mutator methods


// indexOf method -- gets the index of a specific item from the beginning

let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is: " + index_of_lenovo);



// lastIndexOf method -- used to find the last occurrence of a specified element within an array. It returns the index of the last occurrence of the element, or -1 if the element is not found in the array. (if naai ngbalik na items sa array).
let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of Lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);




// slice method -- removes items from an array from the indicated index til the end of the array

let hobbies = ["Gaming", "Running", "Cheating", "Cycling", "Writing"];

let sliced_array_from_hobbies = hobbies.slice(2);

console.log(hobbies);

console.log("Sliced items from the array:");
console.log(sliced_array_from_hobbies);



// slice method -- which limits the slice, meaning, it does not remove til the end of the array
// 2 -- start of slice, 3 -- "dead-end". still remains from the list
let sliced_array_from_hobbies_B = hobbies.slice(2, 3);

console.log(hobbies);
console.log("Sliced items from the array with limit:");
console.log(sliced_array_from_hobbies_B); // output: "Cheating"




// slice method -- removes the last 3 items from the array
let sliced_array_from_hobbies_C = hobbies.slice(-3);

console.log(hobbies);
console.log("Sliced 3 items from the end of the array:");
console.log(sliced_array_from_hobbies_C);






// toString method

let string_array = hobbies.toString();
console.log(string_array);
console.log(string_array.toString());
// these two outputs the same
// output: Gaming,Running,Cheating,Cycling,Writing






// concat method

let greeting = ["hello", "world"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);

console.log(concat_greeting);
console.log(greeting.concat(exclamation));
// these two outputs the same stuff as well
// output: ['hello', 'world', '!', '?']





// join method -- converts the array to a string, and inserts the specified argument (in this case, a separator) between each item

console.log(hobbies.join(", "));
// output: Gaming, Running, Cheating, Cycling, Writing





// forEach method -- used to iterate through an array from the very first index until the last item. this does not return an array

hobbies.forEach(function(hobby) {
	console.log(hobby);
});

// console.log(hobbies.forEach()); -- this is an error





// map method -- loops same as the forEach, but it returns a new updated array

let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function(number) {
	return number * 2;
})

console.log(numbers_map);

// mag error if forEach ang gamit insterad of map






// filter method -- returns a new array
// it always has a condition (in this case, < 3), to filter the items
// common use -- search functionality

let filtered_numbers = numbers_list.filter(function(number) {
	return (number < 3);
})

console.log(filtered_numbers);








// [SECTION] multi-dimensional arrays / aka nested array

let chess_board = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

// [1] means the row; [4] means the column; e2
console.log(chess_board[1][4]);

