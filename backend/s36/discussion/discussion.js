//  $ signs is needed to read the value

// using the aggregate method
db.fruits.aggregate([
	// $match - is used to match or get documents that satisfies the condition
	// $match is similar to find()
	// you can use query operators to make your criteria more flexible (e.g. $gt, $lt)
	/*
		$match -> Apple, Kiwi, Banana
	*/
	{ $match: {	onSale: true }},

	// $group - allows us to group together documents, and create an analysis out of the group elements
	// _id: $supplier_id
	/*
		Apple = 1.0
		Kiwi = 1.0
		Banana = 2.0

		_id: 1.0
			Apple, Kiwi

			total: sum of the fruit stock of 1.0
			total: Apple stocks + Kiwi stocks
			total: 20			+ 25
			total: 45

		_id: 2.0
			Banana

			total: sum of the fruit stock of 2.0
			total: Banana stock
			total: 15
	*/
	// $sum (predefined) - used to add or total the values in a given field
  	{ $group: { _id: "$supplier_id", total: { $sum : "$stock"}}}
]);

// output
{
_id: 2,
total: 15
}
{
_id: 1,
total: 45
}





db.fruits.aggregate([
	/*
		$match - Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true }},

	/*
		Apple - 1.0
		Kiwi - 1.0
		Banana - 2.0

		_id: 1.0
			avgStocks: average stocks of fruits in 1.0
			avgStocks: (Apple stocks + Kiwi stocks) / 2
			avgStocks: (20 + 25) / 2
			avgStocks: 22.5

		_id: 2.0
			avgStocks: average stocks of fruits in 2.0
			avgStocks: Banana stocks / 1
			avgStocks: 15 / 1
			avgStocks: 15
	*/

	/*
		{
			_id: 1.
			avgStocks: 22.5
		}
		{
			_id: 2,
			avgStocks: 15
		}
	*/

	// $avg - gets the average of the values of the given field per group
	{ $group: { _id: '$supplier_id', avgStocks: { $avg: '$stock' }}},
	
	// $project - can be used when "aggregating" data to include/exclude fields from the returned results
	// field projection

	/*
		{
		  avgStocks: 15
		}
		{
		  avgStocks: 22.5
		}
	*/
	{ $project: { _id: 0 }}
]);





db.fruits.aggregate([
	/*
		$match - Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true }},

  /*
		Apple - 1.0
		Kiwi - 1.0
		Banana - 2.0

		_id: 1.0
			maxPrice: finds the highest price of fruit
			maxPrice: Apple price vs Kiwi price
			maxPrice: 40 vs 50
			maxPrice: 50

		_id: 2.0
			maxPrice: finds the highest price of fruit
			maxPrice: Banana price
			maxPrice: 20
	*/

  	/*
		{
			_id: 1.
			maxPrice: 50
		}
		{
			_id: 2,
			maxPrice: 20
		}
	*/

	{ $group: { _id: "$supplier_id", maxPrice: { $max: "$price" }}},
	
	// $sort - to change the order of the aggregated result (either 1 or -1)
	// providing a value of -1 will sort the aggregated results in a reverse order
	{ $sort: { maxPrice: -1 }}
]);







db.fruits.aggregate([
	// $unwind - deconstructs the array field from a collection with an array value to output result for each element
	{ $unwind: "$origin" },
]);






db.fruits.aggregate([
  { $unwind: '$origin' },
  { $group: { _id: '$origin', kinds: { $sum: 1 }}}
]);