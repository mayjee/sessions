// ARGUMENTS and PARAMETERS

function printName(name) {
	console.log("I'm the real " + name);
}

printName("Slim Shady");



function checkDivisibilityBy2(number) {
	let result = number % 2;

	console.log("The remainder of " + number + " is " + result);
};

checkDivisibilityBy2(10);






// MULTIPLE ARGUMENTS and PARAMETERS

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " +lastName);
}

createFullName("Naruto", "Uzumaki", "Namikaze");


createFullName("Naruto", "Uzumaki") // output: Naruto Uzumaki undefined




// if sobra arguments mo, kai wa ka ngbantay

// function createFullName(firstName, middleName) {
// 	console.log(firstName + " " + middleName + " " + lastName);
// }


// createFullName("Naruto", "Uzumaki", "Namikaze");
// output: Uncaught ReferenceError: lastName is not defined
//     at createFullName (script.js:38:50)
//     at script.js:30:1
// createFullName @ script.js:38
// (anonymous) @ script.js:30


// USAGE OF PROMPTS AND ALERTS
let user_name = prompt("Enter your username");

function displayWelcomeMessageForUser(userName) {
	alert("Welcome to the Black Parade, " + userName);
}

// may use user_name in all of them
// function displayWelcomeMessageForUser(user_name) {
// 	alert("Welcome to the Black Parade, " + user_name);
// }

displayWelcomeMessageForUser(user_name);