// Server variables for initialization
// importing express
const express = require('express');

// invoking/initializes express()
const app = express();
const port = 4000;




// Middleware - function that runs in the application, it runs in between applications to handle commanded functionalities
// registering express.json() as a middleware
// express.json() is responsible for the express application to read the 'body' when there are routes and there are requests with body being sent. it is responsible for converting every json format into regular JS. it parses json request
app.use(express.json());

// by default, any express application, if it receives data, it can only receive a string or an array. this function allows it to receive/read other data types
app.use(express.urlencoded({extended: true}));




// Server listening
// to listen to a specific port
app.listen(port, () => console.log(`Server is running at port ${port}`));




// [SECTION] Routes

app.get('/', (request, response) => {
	response.send('Hello World');
})


app.post('/greeting', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
})



// mock database
let users = [];

app.post('/register', (request, response) => {
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);

		response.send(`User ${request.body.username} has successfully been registered.`);
	} else {
		response.send('Please input BOTH username and password.');
	}
})



// for getting all of the registered users / getting the list/array of users
app.get('/users', (request, response) => {
	response.send(users);
})






module.exports = app;