// Greater than operator
db.users.find({
	age: {
		$gte: 82
	}
});
// gte --  greater than or equal




// Less than operator
db.users.find({
	age: {
		$lte: 82
	}
});




// Regex operator
db.users.find({
	firstName: {
		$regex: 'S'
	}
});


db.users.find({
	firstName: {
		$regex: 's',
		$options: 'i'
	}
});



db.users.find({
	lastName: {
		$regex: 'T'
	}
});


db.users.find({
	lastName: {
		$regex: 'T',
		$options: 'i'
	}
});




// Combining operators
db.users.find({
	age: {
		$gt: 70
	},
	lastName: {
		$regex: 'g'
	}
});



db.users.find({
	age: {
		$lt: 78
	},
	lastName: {
		$regex: 'g'
	}
});



db.users.find({
	age: {
		$lte: 76
	},
	firstName: {
		$regex: 'j',
		$options: 'i'
	}
});






// Field projection
db.users.find({}, {
	"_id": 0
});



db.users.find({}, {
	"firstName": 1
});
// outputs the first names and the _id



db.users.find({}, {
	"_id": 0,
	"firstName": 1
});