// Continuation of Array Iteration Methods
// Array only has iteration methods for now, in contrast with objects
// Array is a predefined object, since the key is already defined through the index


// Iteration Methods

/*
	-- loops through all the elements to perform repetitive tasks on the array


	ex:
	forEach() -- to loop through the array

	map() -- loops through the array, and returns a new array

	filter() -- returns a new array containing elements which meets the given condition
*/




// every() -- checks if all elements meet the given condition
/*
	returns "true" if all elements meet the given condition, but "false" if not
*/


let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {

	return number > 3;

});

console.log("Result of every() method:");
console.log(allValid);
// output: false






// some() -- checks if at least one element meets the given condition
/*
	returns false if all the elements do not meet the given condition
*/

let someValid = numbers.some(function(number) {

	return number < 1;
});

console.log("Result of some() method:");
console.log(someValid);





// methods can be "chained" using them one after another
// includes() method

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a");
});

console.log(filteredProducts);






// reduce() method
/*
	accumulator - accumulated value

	has to have 2 parameters, accumulated and current

	acts similar to concat method if used on strings (may use toString, and concat)
	
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {

	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("Current value: " + y);

	return x + y;
})

console.log("Result of reduced method: " + reducedArray);



let productsReduced = products.reduce(function(x, y) {

	return x + ", " + y;
});

console.log("Result of reduced() method: " + productsReduced);