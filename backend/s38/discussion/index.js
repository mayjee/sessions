let http = require('http');

const port = 4000;


// Mock database
let users = [
		{
			"name": "Paolo",
			"email": "paolo@email.com"
		},
		{
			"name": "Shinji",
			"email": "shinji@email.com"
		}
	];



const app = http.createServer((req, res) => {

	// /items GET route
	if(req.url == '/items' && req.method == 'GET') {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Data retrieved from the database')

	}


	// /items POST route
	if (req.url == '/items' && req.method == 'POST') {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Data sent to the database');

	}


	// getting all items from mock database
	if(req.url == '/users' && req.method == 'GET') {

		res.writeHead(200, {'Content-Type': 'application/json'});

		res.end(JSON.stringify(users));

	}


	// creating a user in our database
	if(req.url == '/users' && req.method == 'POST') {

		// placeholder for the data that is within the request
		let request_body = '';

		// the 'on' function listens for specific behavior within the request. in this example, we are checking for when the request detects data within it. once the request receives data, it will run a function that will handle that data in its parameter
		req.on('data', (data) => {
			request_body += data; //assigns the data from the request to the 'request_body' variable
		});

		// req.end() cannot contain multiple lines of code
		req.on('end', () => {

			// to check the data type of the 'request_body' variable
			console.log(typeof request_body); //stringified json  by default / appears string on terminal

			// converting the JSON string into regular JS format
			request_body = JSON.parse(request_body); //reassigned into a parsed request_body, turning into regular JS array, to access the name and email

			// creating a new user object to contain the data from the request_body
			let new_user = {
				"name": request_body.name, // the (.) dot notation is possible since the request_body has been converted to JS format
				"email": request_body.email
			}

			// push() the new_user object into the mock database array
			users.push(new_user);

			res.end(JSON.stringify(new_user));

		})

	}


});

app.listen(port, () => console.log(`Server is running at localhost:${port}`));
// empty '()' is needed if the console.log is in the second argument

