// fetch() returns 'promise'. it sends a request to the API/link
// promise - can have diff status (pending - no data yet; resolved - when there is data)
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));
// output: Promise {<pending>} with a dropdown arrow at its left


// PROMISE CHAIN
// synchronous programming
// the fethc() function returns a promise which can then be chained using the then() function. The then() function waits for the promise to be resolved before executing code
fetch('https://jsonplaceholder.typicode.com/posts')
	// 'response' carries the data returned by the API
	// .then(response => console.log(response.status)); // output: 200 -meaning okay
	.then(response => response.json()) // converts the json string from the response into regular JS format
	// the response.json() is passed to the 'posts'
	.then(posts => console.log(posts));



// below is not ideal. prone to error as it runs the 2nd line even when there is still no data returned in the first line, esp when internet speed is slow

/*
function fetchData() {
	let result = fetch('https://jsonplaceholder.typicode.com/posts')

	let json_result = result.jason();

	console.log(json_result);
}


improved syntax below
*/



// asynchronous programming
// As of ES6, the async/await syntax can be used to replace the .then() convention as it helps the function to be asynchronous and accomplish...
async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	let json_result = await result.json();

	console.log(json_result);
}

fetchData();




// Adding headers, body, and method to the fetch() function
// CREATING NEW POST
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	// 'body' should be included when adding a new data into an existing API or updating an existing data
	body: JSON.stringify({
		title: "New post!",
		body: "Hello World.",
		userId: 2
	})
})

	.then(response => response.json())
	.then(created_post => console.log(created_post));




// UPDATING EXISTING POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	// 'body' should be included when adding a new data into an existing API or updating an existing data
	body: JSON.stringify({
		title: "Corrected Post!"
	})
})

	.then(response => response.json())
	.then(updated_post => console.log(updated_post));




// DELETING EXISTING POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
	
	.then(response => response.json())
	.then(deleted_post => console.log(deleted_post));




// FILTERING POSTS
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	.then(response => response.json())
	.then(post => console.log(post));




// GETTING COMMENTS OF A POST
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then(response => response.json())
	.then(comments => console.log(comments));

