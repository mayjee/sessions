// JSON format example
// {
// 	"city": "Pateros",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }



// [SECTION] JSON Arrays
// assignment operator is colon (:)
/*
"cities": [
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Batangas City",
		"province": "Batangas",
		"country": "Philippines"
	},
	{
		"city": "Star City",
		"province": "Pasay",
		"country": "Philippines",
		"rides": [
			{
				"name": "Star Flyer"
			},
			{
				"name": "Gabi ng Lagim"
			}
		]
	}
]
*/





// [SECTION] JSON methods
// JS array to JSON
let zuitt_batches = [
	{ batchName: "303" },
	{ batchName: "271" }
]

// before stringification, JS reads the variable as a regular JS array
console.log("Output before stringification:");
console.log(zuitt_batches);

// after the JSON.stringify function, JS now reads the variable as a string (equivalent to converting the array into JSON format)
console.log("Output after stringification:");
console.log(JSON.stringify(zuitt_batches));




// user details
let first_name = prompt("What is your first name?");
let last_name = prompt("What is your last name?");

// the stringify function/method converts the JS object/array into JSON
let other_data = JSON.stringify({
	firstName: first_name,
	lastName: last_name
})

console.log(other_data);







// [SECTION] convert strigified JSON into JA objects/arrays
let other_data_JSON = `[{ "firstName": "Itachi", "lastName": "Uchiha" }]`;

// the parse function/method converts the JSON string into a JS object/array
let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);

// since the JSON is converted, you can now access the properties in the regular JS fashion
console.log(parsed_other_data[0].firstName);

