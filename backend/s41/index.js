// Server variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config(); // initialization of the .env package
const app = express();
const port = 4000;



// MongoDB Connection
// connection string from MongoDB atlas Drivers -- node.js -- 5.5
// password saved in .env file
// password: admin user password
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-villaver.p9gbwoa.mongodb.net/b303-todo?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true, // to set the new URL parser for the connection string
		useUnifiedTopology: true

});



// mongoose.connection.on('error', () => console.log('Connection error :D'));
// mongoose.connection.once('open', () => console.log('Connected to MongoDB'));


// improved
let database = mongoose.connection;

database.on('error', () => console.log('Connection error :D'));
database.once('open', () => console.log('Connected to MongoDB'));



// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// [SECTION] Mongoose Schema
// Schema is a class where we can put in object
// one schema per collection/table
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


// for the activity
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})



// [SECTION] Models - representation of the collection
// naming convention: capital first letter, singular form, but output will be "tasks"
// other example: Post -> posts
// taskSchema in the 2nd argument is for the .model() to know which schema is to be followed
const Task = mongoose.model("Task", taskSchema);


// for the activity
const User = mongoose.model("User", userSchema);



// [SECTION] Routes

// Creating a new task
app.post('/tasks', (request, response) => {

	Task.findOne({name: request.body.name}).then((result, error) => {
		// Check if the task already exists but utilizing the 'name' property. if it does, a response will be returned to the user
		if (result != null && result.name == request.body.name) {

			return response.send("Duplicate task found!");

		} else {
			// 1. Create a new instance of the task model which will contain the properties required based on the schema. not saved in the databse yet
			let newTask = new Task({
				name: request.body.name
			});

			// 2. Save the new task to the database
			newTask.save().then((savedTask, error) => {
				if (error) {
					return response.send({
						message: error.message //the message of the error itself, built-in property
					});
				}

				return response.send(201, 'New task created!');
			})
		}
	}) 
})


/*
in postman:
1. s41-discussion
2. tasks folder
3. request: post, with name in the body


in mongoDB atlas:
1. browse collections
2. b303-todo -- tasks
3. the tasks made, {"name": "watch JJK and BSD"} has now reflected in the database
*/



// Getting all of the tasks
app.get('/tasks', (request, response) => {
	// .find(), .findOne(), many to mention sila tanan
	Task.find({}).then((result, error) => {
		// if there is an error, the next code will not execute
		if (error) {
			return response.send({
				message: error.message
			})
		}

		// .json - to return .json format from the database properly
		// response.status(200).json(), almost same as response.send(200, {})
		return response.status(200).json({
			tasks: result
		})
	})
})



// for the activity
// register user

app.post('/signup', (request, response) => {

	User.findOne({username: request.body.username}).then((result, error) => {

		if (result != null && result.username == request.body.username) {

			return response.send('Duplicate username found.')

		} else {

			// checks if username and password are empty
			if (request.body.username == '' && request.body.password == '') {

				return response.send('BOTH username and password must be provided');

			}

			// if not empty, then proceed with registration
			let newUser = new User ({
				username: request.body.username,
				password: request.body.password
			});


			newUser.save().then((savedUser, error) => {

				if (error) {
					return console.error(error.message);
				}

				return response.send(201, 'New  created!');
			})

		}

	})
})












// Server listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`));




module.exports = app;