// [SECTION] while loop

// let count = 5;

// while (count !== 0) {
// 	console.log("Current value of count: " + count);
// 	count--;
// };





// [SECTION] do-while loop

// let number = Number(prompt("Give me a number: "));
// Number converts the value of prompt into a number data type, as prompt valule is always a string


// do {
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while (number < 10);





// [SECTION] for loop

// for (let count = 0; count <= 20; count++) {
// 	console.log("Current for-loop value: " + count);
// };


// let my_string = "suguru";

// console.log(my_string.length);

// to get print a specific character in a string, in this case, the "g"
// console.log(my_string[2]);


// loops though each letter in the string, and will keep iterating as long as you love me... as the current index is less than the length of the string
// for (let index = 0; index < my_string.length; index++) {
// 	console.log(my_string[index]);
// };



// [MINI ACTIVITY]
// loop through the 'my_name' variable which has a string with your name on it.
// display each letter in the console but EXCLUDE all the vowels from it




// let my_name = "Jenny Vee Villaver";

// function myName() {
//   let vowels = ['a', 'e', 'i', 'o', 'u'];
  
//   for (let index = 0; index < my_name.length; index++) {
//     let consonants = my_name[index];
    
//     if (!vowels.includes(consonants)) {
//       console.log(consonants);
//     };
//   };
// };

// myName();



// my effing answer that's wrong

/*
let namae = "Jenny Vee Villaver";

let vowels = ["a", "e", "i", "o", "u"];

for (let index = 0; index < namae.length; index++) {
	if (namae[index] !== vowels) {
		console.log(namae[index]);
	}
} 
*/



//solution from teacher

// let my_name = "Jenny Vee Villaver";

// for (let index = 0; index < namae.length; index++) {
// 	if (
// 		my_name[index].toLowerCase() == "a" ||
// 		my_name[index].toLowerCase() == "e" ||
// 		my_name[index].toLowerCase() == "i" ||
// 		my_name[index].toLowerCase() == "o" ||
// 		my_name[index].toLowerCase() == "u"
// 	) {
// 		console.log("")
// 	} else {
// 		console.log(my_name[index]);
// 	}
// }




// [SECTION] continue and break
// let my_name = "Jenny Vee Villaver";

// for (let index = 0; index < my_name.length; index++) {
// 	if (
// 		my_name[index].toLowerCase() == "a" ||
// 		my_name[index].toLowerCase() == "e" ||
// 		my_name[index].toLowerCase() == "i" ||
// 		my_name[index].toLowerCase() == "o" ||
// 		my_name[index].toLowerCase() == "u"
// 	) {
// 		// if we use the 'continue' keyword, it will skip the else block and reiterate the loop to check if the next letter is a vowel
// 		continue;
// 	} else {
// 		console.log(my_name[index]);
// 	}
// }



/*
let name_two = "suguru";

for (let index = 0; index < name_two.length; index++) {
	console.log(name_two[index]);

	if (name_two[index].toLowerCase() == "u") {
		console.log("Skipping...");
		continue;
	}

	if (name_two[index] == "r") {
		break;
	}
}
*/



// difference
let name_two = "suguru";

for (let index = 0; index < name_two.length; index++) {

	if (name_two[index].toLowerCase() == "u") {
		console.log("Skipping...");
		continue;
	}

	if (name_two[index] == "r") {
		break;
	}

	console.log(name_two[index]);
}