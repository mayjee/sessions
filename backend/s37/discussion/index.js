// use 'require' directive commands to load the node.js modules
//  a module is a software component or part of a program that contains one or more routines
// modules are predefined
// the 'http' module lets node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL
// HTTP is a protocol that allows the fetching of resources like HTML documents
let http = require("http");

// 'createServer' method creates an HTTP server that listens to requests on a specific port and gives the response back to the client
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// 4000 is the port where the server will listen to
// a port is a virtual point where the network connections start and end
// in creating a server, 'request' and 'response' are always present. first argument should always be the client request, and the second arguments will always be the server response

http.createServer(function(request, response) {

	// 'writeHead' method is used what type of response we are trying to send
	// 'writeHead' has 2 arguments: 200 - status code for the response - means OK
	// 'Content-Type' sets the content type of the response to be a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// 'end' sends the response with the text content, 'Hello World'
	response.end('Hello World');

}).listen(4000)

// when server is running, console will print the message
console.log('Server is running at port 4000');




// to close server: ctrl + c
// make sure to abort the server before closing



