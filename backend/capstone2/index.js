// Server variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = process.env.PORT || 4000;
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const app = express();




// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());



// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);



// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-villaver.p9gbwoa.mongodb.net/b303-capstone2?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

});


mongoose.connection.on('error', () => console.log("Cannot connect to database."));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));



app.listen(port, () => {

	console.log(`Capstone2 E-commerce API is now running at localhost:${port}`);
});


module.exports = app;