const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;


// Add/create a product (admin only)
router.post('/add-new', auth.verify, auth.verifyAdmin, (request, response) => {

	ProductController.addProduct(request, response)
})



// Retrieve all products
router.get('/all', (request, response) => {

	ProductController.getAllProducts(request, response);
});



// Retrieve all active products
router.get('/', (request, response) => {

	ProductController.getAllActiveProducts(request, response);
})



// Retrieve a specific/single product
router.get('/:id', (request, response) => {

	ProductController.getProduct(request, response);
})



// Update product information (admin only)
router.put('/:id', verify, verifyAdmin, (request, response) => {

	ProductController.updateProduct(request, response);
})



// Archive Product (Admin only)
router.put('/:id/archive', verify, verifyAdmin, (request, response) => {

	ProductController.archiveProduct(request, response);
})



// Activate Product (Admin only)
router.put('/:id/activate', verify, verifyAdmin, (request, response) => {

	ProductController.activateProduct(request, response);
})










// search function by course name from chatGPT
// router.post('/search', courseController.searchCourses);

// modified
router.post('/search', (request, response) => {

	CourseController.searchCourses(request, response);
});



// Delete a specific/single course from chatGPT
router.delete('/:id', verify, verifyAdmin, (request, response) => {

	CourseController.deleteCourse(request, response);
});





module.exports = router;