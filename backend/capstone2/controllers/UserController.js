const User = require('../models/User.js');
const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.checkEmailExists = (request_body) => {

	return User.find({email: request_body.email}).then((result, error) => {

		if (error) {

			return error.message
		}

		if (result.length <= 0) {

			return "Email is not registered yet.";
		}

		return "User email already exists.";
	})
}


module.exports.registerUser = (request_body) => {

	let new_user = new User ({

		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {

		if (error) {

			return error.message;
		}

		return "User successfully registered!";

	}).catch (error => console.log(error));
}


module.exports.loginUser = (request, response) => {

	return User.findOne({email: request.body.email}).then(result => {
	
		if (result == null) {

			return response.send({

				message: "The user is not registered yet."
			})
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if (isPasswordCorrect) {

			return response.send({accessToken: auth.createAccessToken(result)});

		} else {

			return response.send({

				message: 'Password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}


module.exports.getAllUsers = (request, response) => {

	return User.find({}).then(result => {

		return response.send(result);
	})
}

// ---------------------------------------------above is done


module.exports.getProfile = (request_body) => {

	return User.findOne({ _id: request_body.id}).then((user, error) => {

		if (error) {
			return {
				message: error.message
			}
		}

		if (user == null) {
	
			return "User does not exist"
		}
		
		user.password = "";

		return user;
	}).catch(error => console.log(error));
}



// uses async, await since there are 2 seaprate collections needed to be modified. waiting for user inputs
module.exports.enroll = async (request, response) => {

	// blocks this function if the user is an admin. if user is admin, the next code will not run
	if (request.user.isAdmin) {

		return response.send('Action forbidden');
	}

	// [SECTION] updating user collection
	// this variable will return true once the user data has been updated
	// awaiting for the user ID, then the details in the new_enrollment
	let isUserUpdated = await User.findById(request.user.id)
	.then(user => {

		let new_enrollment = {

			courseId: request.body.courseId,
			// courseName: request.body.courseName,
			// courseDescription: request.body.courseDescription,
			// coursePrice: request.body.coursePrice
		}

		// push is used since enrollments is an array
		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true)
		.catch(error => error.message);
	})

	// sends any error within 'isUserUpdated' as a response
	if (isUserUpdated !== true) {

		return response.send({message: isUserUpdated});
	}


	// [SECTION] updating course collection
	let isCourseUpdated = await Course.findById(request.body.courseId)
	.then (course => {

		let new_enrollee = {
			userId: request.user.id
		}

		course.enrollees.push(new_enrollee);

		return course.save().then(updated_course => true)
		.catch(error => error.message);
	})

	if (isCourseUpdated !== true) {

		return response.send({message: isCourseUpdated});
	}


	// if both ifUserUpdated and isCourseUpdated are true, this next code will run
	if (isUserUpdated && isCourseUpdated) {

		return response.send({
			message: 'Enrolled successfully'
		});
	}

}



module.exports.getEnrollments = (request, response) => {

	User.findById(request.user.id)
	.then(result => response.send(result.enrollments))
	.catch(error => response.send(error.message))
}



