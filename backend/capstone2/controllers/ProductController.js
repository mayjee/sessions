const Product = require('../models/Product.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


// adding a product (admin only)
module.exports.addProduct = (request, response) => {
    let new_product = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    });

    return new_product.save()
        .then(saved_product => {
            response.send("Product successfully added!");
        })
        .catch(error => {
            response.send(error.message);
        });
};


// retrieving all products
module.exports.getAllProducts = (request, response) => {

	return Product.find({}).then(result => {

		return response.send(result);
	})
}


// retrieving all active products
module.exports.getAllActiveProducts = (request, response) => {

	return Product.find({isActive: true})
	.then(result => {

		return response.send(result);
	})
}


// Retrieve a specific/single product
module.exports.getProduct = (request, response) => {

	return Product.findById(request.params.id)
	.then(result => {
		return response.send(result);
	})
}


// Update product information (admin only)
module.exports.updateProduct = (request, response) => {

	let updated_product_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details)
	.then(() => response.send('Product updated successfully!'))
	.catch(error => response.send(error.message));
};


// Archive product (Admin only)
module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: false })
	.then(() => response.send("Product successfully moved to archive."))
	.catch(error => response.send(error.message));
};


// Activate Product (Admin only)
module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: true })
	.then(() => response.send("Product succesfully activated!"))
	.catch(error => response.send(error.message));
};




// search function from chatGPT
/*
searchCourses: async (req, res) => {
    try {
      const courseName = req.body.courseName;
      const courses = await Course.find({ name: { $regex: courseName, $options: 'i' } });
      res.status(200).json({ courses });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for courses.' });
    }
  },
*/

// modified
module.exports.searchCourses = (request, response) => {
      const courseName = request.body.courseName;

      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      	message: error.message
      }))
}

  

// deleting a course from chatGPT
module.exports.deleteCourse = (request, response) => {
	Course.findByIdAndDelete(request.params.id)
	.then((course) => {
		if (!course) {
			return response.status(404).send({ message: 'Course not found' });
		}
		return response.send({ message: 'Course deleted successfully' });
	})
	.catch((error) => response.status(500).send({ message: error.message }));
};




