/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/


function getUserInfo() {
	let person = {
		name: "Sherlock Holmes",
		age: 42,
		address: "221B Baker Street, London, United Kingdom",
		isMarried: false,
		petName: "Dr. Watson"
	};

	return person;
};






/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/


function getArtistsArray() {
	let fav_bands = ["GReeeeN", "wacci", "King Gnu", "sumika", "the peggies"];

	return fav_bands;
};

// const artists_array = getArtistsArray();

// console.log(artists_array);




/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/


function getSongsArray() {
	let fav_songs = ["Tabidachi", "Natsu no Ne", "Nagareboshi", "Tabiji", "Sakayume"];

	return fav_songs;
};

// const songs_array = getSongsArray();

// console.log(songs_array);



/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/


function getMoviesArray() {
	let fav_movies = ["Naruto", "Haikyuu", "World Trigger", "Assassination Classroom", "Bungou Stray Dogs"];

	return fav_movies;
};




/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

// function getNumberArray() {
//   const numbersArray = [1, 2, 3, 4, 5];
//   return numbersArray;
// }

// // Call the function to get the array of numbers
// let numbersArray = getNumberArray();

// // console.log(numbersArray);
// console.log(getNumberArray());



function getPrimeNumberArray() {
	let prime_numbers = [2, 3, 5, 7, 11];

	return prime_numbers;
};

// let prime = getPrimeNumberArray();

// console.log(prime);
// console.log(getPrimeNumberArray());







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}