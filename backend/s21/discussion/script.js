// function declaration and invocation

function printName() {
	console.log("My name is Slim Shady");
};

printName();




// FUNCTION EXPRESSION
let variable_function = function() {
	console.log("Hello from function expression");
}

variable_function();




// SCOPING
let global_variable = "Call me Mr. Worldwide Kakashi";

console.log(global_variable);

function showNames() {
	let function_variable = "Joe Biden";
	const function_const = "John Cena";

	console.log(function_variable);
	console.log(function_const);

	// you can use a global variable inside any fucntion as long as they are declared outside of the function scope
	console.log(global_variable);
};

// you cannot use a locally-scoped variables outside the function they are declared in
// console.log(function_variable); -- commented out kai error

showNames();





// NESTED FUNCTION
function parentFunction() {
	let name = "Jane Eyre";

	function childFunction() {
		let nested_name = "John Cena";

		console.log(name);
		console.log(nested_name);
	}

	childFunction();

	// console.log(nested_name); -- error sad  ni sya. haha
}

parentFunction();

// childFunction(); -- mag eerror

// nesting max: 3




// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser() {
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();




// RETURN STATEMENT
function fullName() {
	return "John Cena";
}

console.log(fullName());
// fullName(); -- does not print anything



function fullName() {
	console.log("John Cena");
}

// both prints
fullName();
console.log(fullName());