// 	db.collection.insertOne


db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "0912345678",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});





// Inserting mutiple documents at once
db.users.insertMany([
	{	"firstName": "Randy",
		"lastName": "Orton"
	},
	{
		"firstName": "John",
		"lastName": "Cena"
	}
]);




// [SECTION] Retrieving documents

// Retrieving all the inserted users
db.users.find();


// Retrieving a spcific document from a collection
db.users.find({ "firstName": "Randy" });






// [SECTION] Updating existing documents
db.users.updateOne(
	{
		"_id": ObjectId("64c1c5f119342bc62a8de605")
	},
	{
		$set: {
			"lastName": "Gaza"
		}
	}
);




// Updating multiple documents
db.users.updateMany(
	{
		"lastName": "Orton"
	},
	{
		$set: {
			"firstName": "Trish"
		}
	}
);






// [SECTION] Deleting documents from a collection
db.users.deleteMany({ "lastName": "Doe" });



// Deleting a single document
db.users.deleteOne({ "_id": ObjectId("64c1c5f119342bc62a8de605") });