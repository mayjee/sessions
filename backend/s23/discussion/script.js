// [SECTION] if-else statements

let number = 1;

if (number > 1) {
	console.log("The number is greater than 1.");
} else if (number < 1) {
	console.log("The number is less than 1.");
} else {
	console.log("None of the conditions were true :( bleeehhh");
}



number = 0;

if (number > 1) {
	console.log("The number is greater than 1.");
} else {
	console.log("The number is less than 1.");
}




// falsey values
if (false) {
	console.log("Falsey");
};


if (0) {
	console.log("Falsey");
};

if (undefined) {
	console.log("Falsey");
};




// truthy values
if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

// wai mogawas sa null, whether truthy or falsey






// ternary operators

let result = (1 < 10) ? true : false;
console.log("Value returned from the ternary operator is " + result);


// what this ternary operator means
/*
if (1 < 10) {
	return true;
} else {
	return false;
}


but mg error siya kai illegal daw ang return true, so i think console.log dapat
*/



if (5 == 5) {
	let greeting = "hello";
	console.log(greeting);
};



// [SECTION] switch statement
let day = prompt("What day of the week is it today?").toLowerCase();

switch (day) {
	case "monday":
		console.log("The day today is Monday");
		break;
	case "tuesday":
		console.log("The day today is Tuesday");
		break;
	case "wednesday":
		console.log("The day today is Wednesday");
		break;
	case "thursday":
		console.log("The day today is Thursday");
		break;
	case "friday":
		console.log("The day today is Friday");
		break;
	default:
		console.log("Please input a valid earthling day");
		break;
};

// first letter of the case should be in small letter, as the prompt accepts both small and capital first letters. if the case starts in capital letter, the prompt does not accept if you type the same word with a small letter as its first letter. why? kai nka .toLowerCase() man

// di mogana ang || sa switch statement





// [SECTION] try/catch/finally statements

function showIntensityAlert(windSpeed) {
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch(error) {
		console.log(error.message);
	} finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);

// error.message -- accesses the message, na murag console logged, which is "alerat is not defined" in this
// finally block is not commonly used nowadays
// error is a parameter sa catch block, but the variable name can be changed. this is built-in JS function, and the error is stored in this variable, no matter the variable name.