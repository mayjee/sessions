// Server variables
const express = require('express');
const mongoose = require('mongoose');
// meaning: cross origin resource sharing
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
const courseRoutes = require('./routes/courseRoutes.js');
const app = express();




// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// to allow other clients to send a request, as API default allows only one client to send a request
// this will allow frontend apps to send requests to server
app.use(cors());





// Routes
app.use('/api/users', userRoutes);

app.use('/api/courses', courseRoutes);






// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-villaver.p9gbwoa.mongodb.net/b303-booking-api?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

});


mongoose.connection.on('error', () => console.log("Cannot connect to database."));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));



// will choose either process.env.PORT || port, esp when it is in the cloud and the port 4000 is not available
// process.env.PORT comes from the cloud
// since we're hosting this API in the cloud, the port to be used should be flexible. Hence, the use of the process.env.PORT which will take the port that the cloud server uses if the 'port' variable is not available
app.listen(process.env.PORT || port, () => {

	console.log(`Booking System API is now running at localhost:${process.env.PORT || port}`);
});


module.exports = app;