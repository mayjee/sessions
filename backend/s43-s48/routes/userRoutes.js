const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');



// Check if email exists
router.post('/check-email', (request, response) => {

	// Controller function
	UserController.checkEmailExists(request.body).then((result) => {

		response.send(result);
	});
});



// Register user
router.post('/register', (request, response) => {

	UserController.registerUser(request.body).then((result) => {

		response.send(result)
	})
})




// Login user
router.post('/login', (request, response) => {

	// instead of request.body, (request, response) is used as using .then here would make the code very long in the routes.js, and the logic needs be coded here. it is in the controller instead
	UserController.loginUser(request, response);
})




// Get user details
// Get profile by id in request.body
/*
router.post('/details', (request, response) => {

	UserController.getProfile(request.body).then((result) => {

		response.send(result)
	})
})
*/


// updated
// auth.verify is from the module.exports.verify in auth.js (a connection)
// auth.verify serves as middleware function within the route
// reads auth.verify first when the request goes to /details, then it moves on to the 3rd argument when next in the auth.js is called
// (request, response) won't run unless ngnext sa auth.verify is called/read
router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {

	UserController.getProfile(request.body).then((result) => {

		response.send(result)
	})
})



// Enroll user to a course
router.post('/enroll', auth.verify, (request, response) => {

	UserController.enroll(request, response);
})


// Get user enrollments
router.get('/enrollments', auth.verify, (request, response) => {

	UserController.getEnrollments(request, response);
})





module.exports = router;