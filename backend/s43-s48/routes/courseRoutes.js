const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js');
const auth = require('../auth.js');


// destructuring verify and verifyAdmin in order to directly use verify and verifyAdmin instead of auth.verify and auth.verifyAdmin
// usually done irl
const {verify, verifyAdmin} = auth;


// my answer s45
// router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {

// 	CourseController.addCourse(request.body).then((result) => {

// 		response.send(result)
// 	})
// })


// s45 solution
// adding a course if admin

// Create single course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {

	CourseController.addCourse(request, response)
})




// s46 - Courses API CRUD
// Get all courses

// router.get('/all', CourseController.getAllCourses); -- another way

router.get('/all', (request, response) => {

	CourseController.getAllCourses(request, response);
});

// may add middleware, auth.verify, to make only the admin to be able to get all the courses




// Get all active courses
router.get('/', (request, response) => {

	CourseController.getAllActiveCourses(request, response);
})




// Get a specific/single course
router.get('/:id', (request, response) => {

	CourseController.getCourse(request, response);
})



// updating a course
router.put('/:id', verify, verifyAdmin, (request, response) => {

	CourseController.updateCourse(request, response);
})




// archiving a course
router.put('/:id/archive', verify, verifyAdmin, (request, response) => {

	CourseController.archiveCourse(request, response);
})


// activating a course
router.put('/:id/activate', verify, verifyAdmin, (request, response) => {

	CourseController.activateCourse(request, response);
})


// search function by course name from chatGPT
// router.post('/search', courseController.searchCourses);

// modified
router.post('/search', (request, response) => {

	CourseController.searchCourses(request, response);
});



// Delete a specific/single course from chatGPT
router.delete('/:id', verify, verifyAdmin, (request, response) => {

	CourseController.deleteCourse(request, response);
});





module.exports = router;