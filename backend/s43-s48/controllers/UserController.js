const User = require('../models/User.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



module.exports.checkEmailExists = (request_body) => {

	return User.find({email: request_body.email}).then((result, error) => {

		if (error) {

			return {
				message: error.message
			}
		}

		if (result.length <= 0) {

			return false;
		}

		// this will only return 'true' if there are no errors AND there is a result in the query or there is an existing user from the database
		return true;
	})
}



module.exports.registerUser = (request_body) => {

	let new_user = new User ({

		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		// 10 -- how many times the password will be hashed/encrypted
		// the higher the no., the more secure, but the performance/decryption will be compromised
		password: bcrypt.hashSync(request_body.password, 10)
		// password: request_body.password
	});

	return new_user.save().then((registered_user, error) => {

		if (error) {

			return {

				message: error.message
			};
		}

		return {

			message: "User successfully registered!",
			// data: registered_user
		};

	}).catch (error => console.log(error));
}



module.exports.loginUser = (request, response) => {

	return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if (result == null) {

			return response.send({

				message: "The user is not registered yet."
			})
		}

		// if a user was found with an existing email, then check if the password of that user matched the input from the request body
		// includes decrypting the password
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if (isPasswordCorrect) {

			// if the password comparison returns true, then respond with the newly generated jwt access token
			return response.send({accessToken: auth.createAccessToken(result)});

		} else {

			return response.send({

				message: 'Password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}




module.exports.getProfile = (request_body) => {

	return User.findOne({ _id: request_body.id}).then((user, error) => {

		if (error) {
			return {
				message: error.message
			}
		}

/*
		if (user == null) {
	
			return "User does not exist"
		}
*/
		user.password = "";

		return user;
	}).catch(error => console.log(error));
}



// uses async, await since there are 2 seaprate collections needed to be modified. waiting for user inputs
module.exports.enroll = async (request, response) => {

	// blocks this function if the user is an admin. if user is admin, the next code will not run
	if (request.user.isAdmin) {

		return response.send('Action forbidden');
	}

	// [SECTION] updating user collection
	// this variable will return true once the user data has been updated
	// awaiting for the user ID, then the details in the new_enrollment
	let isUserUpdated = await User.findById(request.user.id)
	.then(user => {

		let new_enrollment = {

			courseId: request.body.courseId,
			// courseName: request.body.courseName,
			// courseDescription: request.body.courseDescription,
			// coursePrice: request.body.coursePrice
		}

		// push is used since enrollments is an array
		user.enrollments.push(new_enrollment);

		return user.save().then(updated_user => true)
		.catch(error => error.message);
	})

	// sends any error within 'isUserUpdated' as a response
	if (isUserUpdated !== true) {

		return response.send({message: isUserUpdated});
	}


	// [SECTION] updating course collection
	let isCourseUpdated = await Course.findById(request.body.courseId)
	.then (course => {

		let new_enrollee = {
			userId: request.user.id
		}

		course.enrollees.push(new_enrollee);

		return course.save().then(updated_course => true)
		.catch(error => error.message);
	})

	if (isCourseUpdated !== true) {

		return response.send({message: isCourseUpdated});
	}


	// if both ifUserUpdated and isCourseUpdated are true, this next code will run
	if (isUserUpdated && isCourseUpdated) {

		return response.send({
			message: 'Enrolled successfully'
		});
	}

}



module.exports.getEnrollments = (request, response) => {

	User.findById(request.user.id)
	.then(result => response.send(result.enrollments))
	.catch(error => response.send(error.message))
}

/*
or

module.exports.getEnrollments = (request, response) => {

	User.findById(request.user.id)
	.then(user => response.send(user.enrollments))
	.catch(error => response.send(error.message))
}
*/






//  add if invalid yung courseid sa request na nilagay, di po ba masasave pa rin siya sa user enrollment? -- if statement



