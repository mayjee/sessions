const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// my answer s45
/*
module.exports.addCourse = (request_body) => {

	let new_course = new Course ({

		name: request_body.name,
		description: request_body.description,
		price: request_body.price
		
	});

	return new_course.save().then((added_course, error) => {

		if (error) {

			return {

				message: error.message
			};
		}

		return {

			message: "Course successfully added!",
			data: added_course
		};

	}).catch (error => console.log(error));
}
*/



// s45 solution
// adding a course if admin

module.exports.addCourse = (request,response) => {

	let new_course = new Course ({

		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	});

	return new_course.save().then((saved_course, error) => {

		if (error) {
			return response.send(false);
		}

		return response.send(true);
	}).catch(error => response.send(error));
}


// check in the atlas after






module.exports.getAllCourses = (request, response) => {

	return Course.find({}).then(result => {

		return response.send(result);
	})
}





module.exports.getAllActiveCourses = (request, response) => {

	return Course.find({isActive: true})
	.then(result => {

		return response.send(result);
	})
}




module.exports.getCourse = (request, response) => {

	return Course.findById(request.params.id)
	.then(result => {
		return response.send(result);
	})
}




module.exports.updateCourse = (request, response) => {

	let updated_course_details = {

		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

// .findByIdAndUpdate -- 2 arguments
	return Course.findByIdAndUpdate(request.params.id, updated_course_details)
	.then((course, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}



// s46 my answer
/*
module.exports.archiveCourse = (request, response) => {

	let archived_course = {
		isActive: request.body.isActive
	}

	return Course.findByIdAndUpdate(request.params.id, archived_course)
	.then((course, error) => {
		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: true
		})
	})

}
*/


// group
/*
module.exports.archiveCourse = (request, response) => {
	let archive_course = {
		isActive: request.body.isActive
	}

return Course.findByIdAndUpdate(request.params.id, archive_course).then((result, error) => {
	if(error){
		return response.send (false)
	}


	return response.send (true);
}).catch(error=> response.send(error));
}
*/


// s46 solution
module.exports.archiveCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, { isActive: false }).then((course, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}


// activate course

// group
/*
module.exports.activateCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, {isActive: true}, {new: true}).then((course_activated, error) => {

		if(error) {
			return response.send({
				message: error
			})
		}

		return response.send(true)

	}).catch(error => console.log(error));
}
*/


// s46 solution
module.exports.activateCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, { isActive: true }).then((course, error) => {
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}



// search function from chatGPT
/*
searchCourses: async (req, res) => {
    try {
      const courseName = req.body.courseName;
      const courses = await Course.find({ name: { $regex: courseName, $options: 'i' } });
      res.status(200).json({ courses });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for courses.' });
    }
  },
*/

// modified
module.exports.searchCourses = (request, response) => {
      const courseName = request.body.courseName;

      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      	message: error.message
      }))
}

  

// deleting a course from chatGPT
module.exports.deleteCourse = (request, response) => {
	Course.findByIdAndDelete(request.params.id)
	.then((course) => {
		if (!course) {
			return response.status(404).send({ message: 'Course not found' });
		}
		return response.send({ message: 'Course deleted successfully' });
	})
	.catch((error) => response.status(500).send({ message: error.message }));
};




