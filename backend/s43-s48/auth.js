// auth.js is a controller for the jwt, which contains multiple functions for the jwt

const jwt = require('jsonwebtoken');
const secret_key = "CourseBookingAPIB303";


// Generating a token
module.exports.createAccessToken = (user) => {

	const user_data = {
		// dot notation since these are from the database
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// the jwt.sign() will generate a token using the user data and secret key
	// the empty object/3rd argument is for additional objects for creating token (e.g. expiresAt: )
	return jwt.sign(user_data, secret_key, {});

}



// Verifying a token
// to verify if the token comes from the app itself
module.exports.verify = (request, response, next) => {

	// this will get the token inside the authorization property
	let token = request.headers.authorization;

	// if (typeof token === undefined) di gumagana
	if (token === undefined) {
		return response.send({auth: 'Failed, please include token in the header of the request'})
	}

	console.log(token);


	// removes the Bearer <space> default text, and the remaining will only be the actual token
	// 7 -- means the first 7 characters which are the "Bearer "
	// needed to slice for the token verification
	token = token.slice(7, token.length);

	console.log(token);




	jwt.verify(token, secret_key, (error, decoded_token) => {

		if (error) {
			return response.send({
				auth: 'Failed',
				message: error.message
			})
		}

		console.log(decoded_token);

		// set the value of the request.user to the decoded token which contains the user data
		// request.user can be used globally
		request.user = decoded_token;


		next();

	})
}


// Verifying if user is admin
// after verifying the token
module.exports.verifyAdmin = (request, response, next) => {

	if (request.user.isAdmin) {
		return next();
	}

	return response.send({
		auth: 'Failed',
		message: 'Action forbidden'
	})
}


/*
terminal output:

{
  id: '64cb5c04c65d9096b7163b8d',
  email: 'youcantseemeandmypassword@email.com',
  isAdmin: true,
  iat: 1691119406
}
*/


/*
postman output:

{
    "_id": "64cb5c04c65d9096b7163b8d",
    "firstName": "Felix Anthony",
    "lastName": "Cena",
    "email": "youcantseemeandmypassword@email.com",
    "password": "",
    "isAdmin": true,
    "mobileNo": "09992221111",
    "enrollments": [],
    "__v": 0
}
*/



