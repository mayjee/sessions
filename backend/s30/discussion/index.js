console.log("ES6 Updates");

// Exponent operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);




// Template Literals
/*
	- allows us to write strings without using concatenation
	- greatly helps us with code readability
*/
let name = "Ken";



// using concatenation
let message = 'Hello, ' + name + '! Welcome to Programming!';
console.log('Message without template literals: ' + message);



// using template literals
// backticks are used (``) and ${} for including JS expressions
message = `Hello, ${name}! Welcome to Programming!`;
console.log(`Message with template literals: ${message}`);



// creates multi-line using template literals
const anotherMessage = `
${name} attended a Math competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings amount is ${principal * interestRate}`);






// Array destructuring
const fullName = ["Juan", "Dela", "Cruz"];


// using array indices
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! Nice to meet you.`);



// using array destructuring
/*
const firstName = fullName[0];
const middleName = fullName[1];
const lastName = fullName[2];
*/
const [firstName, middleName, lastName] = fullName;

console.log(`Hello, ${firstName} ${middleName} ${lastName}! Nice to meet you.`);



// Object destructuring
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}



// using the dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello, ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);



// using object destructuring
const {givenName, familyName, maidenName} = person;

console.log(`Hello, ${givenName} ${maidenName} ${familyName}! It's good to see you.`);



// using object destructuring in functions
function getFullName({givenName, maidenName, familyName}) {

	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);





// Arrow functions
const hello = () => {
	console.log("Hello World!");
}

hello();


// Traditional functions
/*
function printFullName(fName, mName, lName) {
	console.log(fName + " " + mName + " " + lName);
}

printFullName("John", "D", "Smith");
*/


// Arrow function with template literals
const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`);
};

printFullName("Jane", "D", "Smith");


// Arrow functions with loops
const students = ["John", "Shawn", "Dwayne"];

// Traditional function
students.forEach(function(student) {
	console.log(`${student} is a student.`);
});

// Arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`);
});



// Implicit Return Statements

// Traditional function
/*
function add(x, y) {
	let result = x + y;
	return result;
};

let total = add(2, 5);
console.log(total);
*/



// Arrow function
// the word "return" is nowhere to be found :D
// single line arrow functions only
// cannot be used with traditional function, but only with arrow function
const add = (x, y) => x + y;

let total = add(2, 5);
console.log(total);




// Default function argument values
// provides a default argument
const greet = (name = "User") => {
	return `Good afternoon, ${name}!`;
}

console.log(greet()); // output: Good afternoon, User!
console.log(greet("Jody")); // output: Good afternoon, Jody!




// [SECTION] Class-based Object Blueprints

// Create a class
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


// Instantiate an object
const fordExplorer = new Car();

// even though the 'fordExplorer' is const, since it is an object, the values can still be reassigned to its properties
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);
// output: Car {brand: 'Ford', name: 'Explorer', year: 2022}



// this logic applies whether you reassign the values of each property separately, ot put them as arguments of the new instance of the class
const toyotaVios = new Car("Toyota", "Vios", 2018);
console.log(toyotaVios);
// output: Car {brand: 'Toyota', name: 'Vios', year: 2018}