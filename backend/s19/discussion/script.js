// alert("Hello World!");

console.log("Hello World!");


// SECTION variables

// variable declaration and invocation
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// concatenation strings
let country = "Fire Nation";
let province = "Konohagakure";
let full_address = province + ', ' + country;
console.log(full_address);


// arithmetic operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Difference: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);


// assignment operators
let assignment_number = 0; // initialized zero as the assignment number

assignment_number = assignment_number + 2; // 0 + 2, equals to 2.
console.log("Result of addition assignment operator: " + assignment_number);

assignment_number += 2;
console.log("Result of shorthand addition assignment operator: " + assignment_number);
