const express = require('express');

// express.Router() establishes the routes, instead of app = express()
const router = express.Router();

// importing controller
const TaskController = require('../controllers/TaskController.js');


// Insert routes here
// TaskController........then(result => {response.send(result); }) is usually the same

// Creating a new task
router.post('/', (request, response) => {

	TaskController.createTask(request.body).then(result => {

		response.send(result);
	})

})



// Getting all of the tasks
router.get('/', (request, response) => {
	
	TaskController.getAllTasks().then(result => {

		response.send(result);
	})

})




// router.delete('/remove')

// http://localhost:4000/api/tasks/remove




// exports routes as a whole
module.exports = router;