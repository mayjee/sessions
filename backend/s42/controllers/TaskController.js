// controller -- where the models are imported
// naming convention -- TaskController or UserController

const Task = require('../models/Task.js');


module.exports.getAllTasks = () => {

	// .find(), .findOne(), many to mention sila tanan
	return Task.find({}).then((result, error) => {
		// if there is an error, the next code will not execute
		if (error) {
			return {
				message: error.message
			}
		}

		return {
			tasks: result
		}
	})
}



module.exports.createTask = (request_body) => {

	return Task.findOne({name: request_body.name}).then((result, error) => {
		// Check if the task already exists but utilizing the 'name' property. if it does, a response will be returned to the user
		if (result != null && result.name == request_body.name) {

			return {
				message: "Duplicate user found!"
			};
			// can be done instead of using response.send()

		} else {
			
			let newTask = new Task({
				name: request_body.name
			});

		
			return newTask.save().then((savedTask, error) => {
				if (error) {
					return {
						message: error.message
					};
				}

				return {

					message: "New task created!"
				};
			})
		}
	})
}