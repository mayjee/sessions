// models -- capital letter, singular
// model and file names must coincide

const mongoose = require('mongoose');


// Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});



// from the models from the prev session
module.exports = mongoose.model("Task", taskSchema);