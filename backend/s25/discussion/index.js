// [SECTION] objects

/*

	an object is a data type that is used to represent real-world objects

	can create properties, and methods/functionalities

*/



// creating objects using initializers or object literals

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);



// creating objects using a constructor function (serves/acts as blueprint)

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
};



// create multiple instance of an object using the "new" keyword
// this method is called instantiation
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop);

let laptop2 = new Laptop("MacBook Air", 2020);
console.log("Result from creating objects using constructor function");
console.log(laptop2);






// [SECTION] accessing object properties

// using the square bracket notation []
console.log("Result from square bracket notation: " + laptop2["name"]);


// using the dot notation
console.log("Result from dot notation: " + laptop2.name);


// accessing array objects
// objects can be placed inside arrays
let array = [laptop, laptop2];

console.log(array[0]["name"]);
console.log(array[0].name);






// [SECTION] adding / deleting / reassigning object properties

// empty object properties are made if the object key and value properties are not set in stone yet
// empty object using object literals
let car = {};

// empty object using constructor function/instantiation
let myCar = new Object(); //capital letter


// adding object properties using dot notation
// objectName.key = "value";
car.name = "Lightning McQueen";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// adding object properties using square bracket notation
car["manufacturing date"] = 2019;
console.log(car["manufacturing date"]);
console.log(car["Manufacturing Date"]);

// object property with space cannot be accessed using dot notation
// console.log(car[car.manufacturing date]);

console.log("Result from adding properties using square bracket notation");
console.log(car);




// deleting object properties
// dot notation can be used to delete properties as well
delete car["manufacturing date"];
console.log("Result from deleting properties: ");
console.log(car);




// reassigning object properties
// can also use square bracket for reassigning, but not best practice
car.name = "Jeep Cherokee";
console.log("Result from reassigning properties: ");
console.log(car);







// [SECTION] object methods

/*
	a method is a function which acts as a property of an object

*/


let person = {
	name: "The Real Slim Shady",
	greet: function() {
		console.log("Hello! My name is " + this.name + "!");
	}
}

console.log(person);
console.log("Result from object methods:");
// greet is now called a method since the function is inside an object
person.greet();



// adding methods to objects
person.walk = function() {
	console.log(this.name + " walked 25 steps forward");
}

person.walk();




let friend = {
	// values can be string, numbers, arrays, object
	name: "Kentarou",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ["ken@gmail.com", "kenjaku@mail.com"],
	introduce: function(person) {
		console.log("Nice to meet you, " + person.name + ". I am " + this.name + " from " + this.address.city + ", " + this.address.state + ".");
	}
}

// person parameter is used to identify that it is from a diff object
// still works even without it
friend.introduce(person);